from django.contrib import admin
from .models import Page

# clase para los campos de solo lectura
class PageAdmin(admin.ModelAdmin):
    readonly_fields=("created_at", "updated_at")

# Register your models here.
admin.site.register(Page,PageAdmin)

TITLE = "App Articulos"
SUBTITLE = "Panel de Gestion"

#panel de gestion
admin.site.site_header = TITLE
admin.site.site_title = TITLE
admin.site.index_title = SUBTITLE

