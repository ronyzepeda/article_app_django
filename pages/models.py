from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.


class Page(models.Model):

    title = models.CharField(max_length=50, verbose_name="titulo")
    content = RichTextField(verbose_name="contenido")
    slug = models.CharField(unique=True, max_length=100,verbose_name="URL amigable")
    visible = models.BooleanField(verbose_name="¿visible?")
    order = models.IntegerField(default=0 ,verbose_name="Orden")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="creado el")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="actualizado el")

    class Meta:
        verbose_name = "pagina"
        verbose_name_plural = "paginas"

    def __str__(self):
        return self.title
