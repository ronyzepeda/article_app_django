from django.shortcuts import render
from blog.models import Category,Article

# Create your views here.
def index_blog(request):

    articles = Article.objects.filter(public = True)
    context ={'title' : "Articulos",
        'articulos': articles}

    return render(request,'blogs/blog.html',context)
